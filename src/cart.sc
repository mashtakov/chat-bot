#Перечисление используемых функций
require: functions.js

theme: /

#Вывод содержимого корзины и общей суммы заказа
    state: Cart
        q!: корзина
        a: Ваша корзина:
        script:
            $temp.totalSum = 0;
            var n = 0;

            for(var i = 0; i < $session.cart.length; i++){
                var current_position = $session.cart[i];

                for(var id = 1; id < Object.keys(pizza).length + 1; id++){

                    if (current_position.name == pizza[id].value.title){

                        var variation = _.find(pizza[id].value.variations, function(variation){
                            return variation.id === current_position.id;
                        });

                        n++;

                        $reactions.answer(n + ". " + current_position.name + ", " + variation.name + "\nЦена: " + variation.price + "\nКоличество: " + current_position.quantity);

                        $reactions.inlineButtons({text: "Удалить", callback_data: current_position.name});

                        $temp.totalSum += variation.price * current_position.quantity;

                    }
                }
            }

            $session.messageId = $request.rawRequest.message.message_id + n + 2;

        a: Общая сумма заказа: {{ $temp.totalSum }} рублей.
        a: Если все верно, отправьте свой номер телефона, и наш менеджер с вами свяжется.
        buttons:
            {text: "Отправить номер телефона", request_contact: true}
            "Меню" -> /ChoosePizza

#Редактирование корзины
        state: Edit
            event: telegramCallbackQuery
            script:
                var name = $request.rawRequest.callback_query.data;
                deleteFromCart(name);
                var message_id = $request.rawRequest.callback_query.message.message_id;

                editText(message_id, 'Удален');
                editText($session.messageId, 'Общая сумма заказа: ' + getTotalSum() + ' руб.');
            if: $session.cart.length == 0
                a: Вы очистили корзину
                go!: /ChoosePizza

#Отправка номера телефона для подтверждения заказа
    state: GetPhoneNumber
        event: telegramSendContact
        script:
            $client.phone_number = $request.rawRequest.message.contact.phone_number;
    