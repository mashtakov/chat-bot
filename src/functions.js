var $months = ['Январь', 'Февраль', 'Март', 'Апрель','Май', 'Июнь', 'Июль', 'Август','Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

//Получаем массив типов справок
function getAppGroups() {
    var $injector = $jsapi.context().injector;
    var $client = $jsapi.context().client;
    var url = $injector.platform.services.api.url+"/local/ajax/applications/get_app_types.php";
    var response = $http.get(url, {
        headers: {
            "Authorization": $injector.platform.services.api.token
        }
    });

    if (response.isOk) {
        $client.arAppGroups = response.data;
        return true;
    }
    else {
        return false;
    }
}

//Получаем массив справок конкретного типа
function getAppTypesByGroupName(groupName) {
    var $client = $jsapi.context().client;
    var arTypes;
    Object.keys($client.arAppGroups).some(function(element, index) {
        if($client.arAppGroups[element].NAME === groupName) {
            arTypes = $client.arAppGroups[element].FORMS;
            return true;
        }
    });
    return arTypes;
}
    
//Получаем id веб-формы по назвавнию типа справки
function getFormIdByAppTypeName(typeName) {
    var $client = $jsapi.context().client;
    var formId;
    var isFinded = false;
    Object.keys($client.arAppGroups).some(function(element, index) {
        Object.keys($client.arAppGroups[element].FORMS).some(function(form, key) {
            if($client.arAppGroups[element].FORMS[form] === typeName) {
                formId = form;
                isFinded = true;
                return true;
            }
        });
        return isFinded;
    });
    return formId;
}

//Получаем массив полей конкретной веб-формы
function getFieldsByFormId(formId) {
    var $injector = $jsapi.context().injector;
    var $client = $jsapi.context().client;
    var url = $injector.platform.services.api.url+"/local/ajax/applications/get_app_fields.php?WEB_FORM_ID="+formId;
    var response = $http.get(url, {
        headers: {
            "Authorization": $injector.platform.services.api.token
        }
    });
    
    if (response.isOk) {
        $client.arAppFields = response.data.QUESTIONS;
        $client.arAppUserData = response.data.USER_DATA;
        $client.arAppForm = response.data.arForm;
        return true;
    }
    else {
        return false;
    }
}

//Получаем вопрос бота относительно кода поля
function generateQuestionByFieldCode(fieldCode, subStep) {
    
    switch(fieldCode) {
        case 'NUMBER_OF_COPIES':
            $reactions.answer('Какое количество копий Вам требуется?');
            break;
        case 'REQUESTED_DATE_FROM':
            if(subStep === 0) {
                $reactions.answer('Выберите месяц начала периода');
                $months.forEach(function(element, index) {
                    $reactions.buttons({text: element});
                });    
            }
            else {
                $reactions.answer('Укажите год начала периода');
            } 
            break;
        case 'REQUESTED_DATE_TO':
            if(subStep === 0) {
                $reactions.answer('Выберите месяц окончания периода');
                $months.forEach(function(element, index) {
                    $reactions.buttons({text: element});
                });    
            }
            else {
                $reactions.answer('Укажите год окончания периода');
            } 
            break;
        case 'APPLICATION_PERIOD':
            $reactions.answer('За какой год требуется справка?');
            break;
        case 'CD_CITY':
            $reactions.answer('Укажите город, в котором была куплена квартира');
            break;
        case 'CD_STREET':
            $reactions.answer('Введите название улицы');
            break;
        case 'CD_HOUSE':
            $reactions.answer('Введите номер дома');
            break;
        case 'CD_APARTMENTS':
            $reactions.answer('Введите номер квартиры');
            break;
        case 'CD_YEAR':
            $reactions.answer('Введите год покупки');
            break;
    }
}

//Отправка заполненной заявки на справку
function sendApp() {
    var $injector = $jsapi.context().injector;
    var $client = $jsapi.context().client;
    var url = $injector.platform.services.api.url+"/local/ajax/applications/get_app_fields.php?WEB_FORM_ID="+$client.order.webFormId;
    var $currentField;
    var $appForm = {};
    
    $appForm.sessid = $client.arAppUserData.SESSID;
    $appForm.WEB_FORM_ID = $client.order.webFormId;
    for(var $i=1; $i<=$client.order.fieldsToFillCount; $i++) {
        $currentField = $client.order.fieldsToFill[$i];
        for(var $j=0; $j<Object.keys($client.arAppFields[$currentField].STRUCTURE).length; $j++) {
            
            $appForm['form_'+$client.arAppFields[$currentField].STRUCTURE[$j].FIELD_TYPE+'_'+$client.arAppFields[$currentField].STRUCTURE[$j].ID] = 
                $client.arAppFields[$currentField].STRUCTURE[$j].ANSWER;
        }
    }
    $appForm.web_form_submit = 'Отправить';
    
    var response = $http.post(url, {
        form: $appForm,
        headers: {
            "Authorization": $injector.platform.services.api.token
        }
    });
    
    if (response.isOk) {
        $reactions.answer('Ваша заявка на справку отправлена! Номер заявки '
            +'<a href="'+$injector.platform.services.api.url+'/applications/?action=view&RESULT_ID='+response.data.RESULT_ID+'" target="_blank">'
            +response.data.RESULT_ID+'</a>'
            +'\nСписок своих заявок Вы можете посмотреть по '
            +'<a href="'+$injector.platform.services.api.url+'/applications/" target="_blank">ссылке</a>');
        return true;
    }
    else {
        $reactions.answer('Ошибка!');
        return false;
    }    
}

//Отправка вопроса компании
function sendCompanyQuestion(message) {
    var $injector = $jsapi.context().injector;
    var $client = $jsapi.context().client;
    var url = $injector.platform.services.api.url+"/local/ajax/jivoform/chat_bot.php";
    
    var response = $http.post(url, {
        body: {
            "message": message
        },
        headers: {
            "Authorization": $injector.platform.services.api.token
        }
    });
    
    if (response.isOk && response.data.code == "SUCCESS") {
        $reactions.answer('Ваш вопрос компании успешно отправлен!');
        return true;
    }
    else {
        $reactions.answer('Ошибка!');
        return false;
    }    
}



