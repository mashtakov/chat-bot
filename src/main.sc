require: functions.js
require: scenarios/misc.sc

require: scenarios/offtopic/offtopic.sc

theme: /

    # Приветствие
    state: start
        q!: * *start
        script:
            getAppGroups();
            $client.order = {};
            $client.order.fieldsToFill = {};
            $client.order.answers = {};
            $client.fieldsFixed = ['PERSONAL_NUMBER', 'APPLICATION_TYPE', 'COMPANY_ID', 'COMMENT', 'RESPONSIBLES'];
            
        a: Здравствуйте! Я Ваш личный помощник! Чем я могу Вам помочь?
        script:
            $reactions.buttons({text: 'Задать вопрос компании', transition: '/get_company_question'});
            $reactions.buttons({text: 'Заказать справку', transition: '/choose_app_group'});
        #go!: /choose_app_group
        
    state: can_i_help
        random:
            a: Могу я вам еще чем-то помочь?
        script:
            $reactions.buttons({text: 'Задать вопрос компании', transition: '/get_company_question'});
            $reactions.buttons({text: 'Заказать справку', transition: '/choose_app_group'});
    
        
    # Задать вопрос компании
    state: get_company_question
        eg!: Main.HaveAQuestion
        a: Какой вопрос Вы хотели бы задать компании?
        
        # Отпрака вопроса компании
        state: send_company_question
            q: $nonEmptyGarbage
            script:
                sendCompanyQuestion($request.query);
            go!:/can_i_help
    
    
    # Выбор группы справок
    state: choose_app_group
        eg!: Main.NeedStatement
        a: Отлично! Для начала выберите, к какой группе относится Ваша справка.
        script:
            Object.keys($client.arAppGroups).forEach(function(element, index) {
                $reactions.buttons({text: $client.arAppGroups[element].NAME, transition: '/choose_app_type'});
            });
        
        state: something
            q: *
            a: Выберите, к какой группе относится Ваша справка
            script:
                Object.keys($client.arAppGroups).forEach(function(element, index) {
                    $reactions.buttons({text: $client.arAppGroups[element].NAME, transition: '/choose_app_type'});
                });
        
    # Выбор типа справки по названию группы    
    state: choose_app_type
        a: Теперь давайте выберем нужную нам справку!
        script:
            $client.lastGroupName = $request.query;
            var appGroupName = $request.query;
            var appTypes = getAppTypesByGroupName(appGroupName);
            Object.keys(appTypes).forEach(function(element, index) {
                $reactions.buttons({text: appTypes[element], transition: '/app_type_fields'});
            });
            
            $reactions.buttons({text: 'Перейти к группам справок', transition: '/choose_app_group'});
            
        state: something
            q: *
            a: Выберите нужную справку
            script:
                var appGroupName = $client.lastGroupName;
                var appTypes = getAppTypesByGroupName(appGroupName);
                Object.keys(appTypes).forEach(function(element, index) {
                    $reactions.buttons({text: appTypes[element], transition: '/app_type_fields'});
                });
                
                $reactions.buttons({text: 'Перейти к группам справок', transition: '/choose_app_group'});
            
    # Отображаем список полей для заполнения
    state: app_type_fields
        script: 
            $client.lastTypeName = $request.query;
            var appTypeName = $request.query;
            $client.order.webFormId = getFormIdByAppTypeName(appTypeName);
            getFieldsByFormId($client.order.webFormId);
            var fieldsList = 'Потребуется заполнить следующие поля:';
            $client.order.fieldsToFillCount = 0;
            var step = 1;
            Object.keys($client.arAppFields).forEach(function(element, index) {
                if($client.fieldsFixed.indexOf(element) == -1) {
                    fieldsList += '\n'+index+'. '+$client.arAppFields[element].CAPTION;
                    $client.order.fieldsToFill[step] = element;
                    step++;
                    $client.order.fieldsToFillCount++;
                }
            });
            
            $client.order.fieldsToFillStep = 1;
            $client.order.fieldsToFillSubStep = 0;
            
            $reactions.answer(fieldsList);
            $reactions.buttons({text: 'Начать заполнение', transition: '/app_fill_process/app_fill_next_field'});
            $reactions.buttons({text: 'Вернуться к выбору группы справок', transition: '/choose_app_group'});
        
        state: something
            q: *
            a: Выберите действие
            script:
                $reactions.buttons({text: 'Начать заполнение', transition: '/app_fill_process/app_fill_next_field'});
                $reactions.buttons({text: 'Вернуться к выбору группы справок', transition: '/choose_app_group'});
            
        
    state: app_fill_process
        state: app_fill_next_field
            script:
                var $currentField = $client.order.fieldsToFill[$client.order.fieldsToFillStep];
                
                //Кол-во подподлей в поле
                $client.order.fieldsToFillSubStepCount = Object.keys($client.arAppFields[$currentField].STRUCTURE).length;
                
                //Генерируем вопрос
                generateQuestionByFieldCode($currentField, $client.order.fieldsToFillSubStep);
                
            go: Answer
                
            state: Answer
                q: $nonEmptyGarbage
                script:
                    var $currentField = $client.order.fieldsToFill[$client.order.fieldsToFillStep];
                    $client.arAppFields[$currentField].STRUCTURE[$client.order.fieldsToFillSubStep].ANSWER = $request.query;
                    
                    if($client.order.fieldsToFillCount === $client.order.fieldsToFillStep 
                       && $client.order.fieldsToFillSubStepCount === $client.order.fieldsToFillSubStep + 1) {
                        $temp.nextStep = 'app_fill_confirm';
                    }
                    else {
                        $temp.nextStep = 'app_fill_next_field';
                        if($client.order.fieldsToFillSubStepCount === 2 && $client.order.fieldsToFillSubStep === 0) {
                            $client.order.fieldsToFillSubStep = 1;
                        }
                        else {
                            $client.order.fieldsToFillStep++;
                            $client.order.fieldsToFillSubStep = 0;
                        }
                    }
                go!: ../../{{$temp.nextStep}}
                
        state: app_fill_confirm
            script:
                var fieldsList = 'Давайте проверим, все ли верно?';
                
                fieldsList += '\n1. '+$client.arAppFields.PERSONAL_NUMBER.CAPTION+': '+$client.arAppUserData.UF_PERSONAL_NUMBER;
                fieldsList += '\n2. '+$client.arAppFields.COMPANY_ID.CAPTION+': '+$client.arAppUserData.COMPANY_NAME;
                fieldsList += '\n3. '+$client.arAppFields.APPLICATION_TYPE.CAPTION+': '+$client.arAppForm.NAME;
                
                Object.keys($client.arAppFields).forEach(function(element, index) {
                    if($client.fieldsFixed.indexOf(element) == -1) {
                        fieldsList += '\n'+(index+3)+'. '+$client.arAppFields[element].CAPTION+':';
                        for(var $i=0; $i<Object.keys($client.arAppFields[element].STRUCTURE).length; $i++) {
                            fieldsList += ' '+$client.arAppFields[element].STRUCTURE[$i].ANSWER;
                        }
                    }
                });    
                $reactions.answer(fieldsList);
            
            state: AnswerYes
                q: Да*
                go!:../../app_fill_finish
                
            state: AnswerNo
                q: Нет*
                a: Попробуем еще раз?
                
                state: AnswerYes
                    q: Да*
                    script:
                        $client.order.fieldsToFillStep = 1;
                        $client.order.fieldsToFillSubStep = 0;
                    go!:../../../app_fill_next_field
                
                state: AnswerNo
                    q: Нет*
                    go!:/hello
                
        state: app_fill_finish
            script:
                sendApp();
            go!:/can_i_help
            
            
    state: CatchAll || noContext = true
        q!: *
        a: Простите, не могу разобрать
        
        