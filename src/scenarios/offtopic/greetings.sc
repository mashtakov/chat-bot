theme: /Offtopic

    state: Greetings
           
        state: GoodBye
            q!: [$AnyWord] [$AnyWord] [$AnyWord] (спокой* ночи|споки|споки-споки|споки споки|спокиспоки) [$AnyWord] [$AnyWord] [$AnyWord] !
            q!: [$AnyWord] [$AnyWord] [$AnyWord] [ладно|все|давай|ну] (до свидан*|до встречи|до завтра|до связи|конец связи|досвидани*|досвидос|дозавтра|прощай|бай бай|гудбай|чау|чао|чмоки чмоки|пока-пока|п о к а|всего (хорошего|доброго)) [$AnyWord] [$AnyWord] [$AnyWord]
            q!: [ладно|все|давай|ну] (увидимся|счастливо|удачи|бывай|пока) !
            q!: * (хорошего|приятного|доброго) * (дня|вечера) [вам|тебе] !
            a: {{$global.OfftopicAnswers["GoodBye"]}}
            go: /

    state: HowCanIHelpYou?
        a: {{$global.OfftopicAnswers["HowCanIHelpYou?"]}}
        
        state: Yes
            q!:  { (*можете|*можешь) [$AnyWord] [$AnyWord] [$AnyWord] помочь } 
            q: * [думаю] (да|*можете|*можешь|надеюсь|хотелось бы) *
            a: {{$global.OfftopicAnswers["CanHelp"]}}

        state: No
            q: * [да] [уже] (ничем|ни чем|нечем|не чем|не надо|не нужно) [спасибо] *
            a: {{$global.OfftopicAnswers["CanNotHelp"]}}

    state: CanIAskYouAQuestion || noContext = true
        q!: * {(могу|можно) [я] (зада*|зада*|спросить|спрошу|поинтерес*) * [вопрос*]} ?
        q!: * {(есть|возник|появился|у меня) * вопрос*} ?
        q!: * $you [може*|сможешь] (ответи*|справи*) (на|с) (люб*|произвольн*) вопрос* *
        q!: * $you [все|ещё] [ещё|еще] (тут|здесь|{[меня] (слушаете|слушаешь|слышите)}) ?
        q!: [$AnyWord] [$AnyWord] {([ещё|еще] один|новый|другой|еще) вопрос} [$AnyWord] [$AnyWord]
        q!: * [я] хотел* спросить
        q!: есть тут кто *
        go!: /Offtopic/HowCanIHelpYou?